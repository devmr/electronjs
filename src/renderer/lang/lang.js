import Vue from 'vue'
import VueI18n from 'vue-i18n'

import fr from './fr.json'
// import en from './en.json'
// import tw from './tw.json'
// import es from './es.json'

Vue.use(VueI18n)

const locale = 'fr'

const messages = {
  fr
}

const i18n = new VueI18n({
  locale,
  messages
})

export default i18n
