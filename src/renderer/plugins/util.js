const baseurl2 = 'https://www.madatsara.com/'
// Store
const Store = require('electron-store')
const electronSettings = new Store()

export default {
  install: (Vue) => {
    Vue.prototype.$helpers = {
      host () {
        let hostSettings = electronSettings.get('host')
        let hostDefault = baseurl2
        let url = typeof hostSettings !== 'undefined' ? hostSettings : hostDefault
        return url
      },
      test () {
        return 'test'
      },
      removeChips: function (item, itemSrc) {
        itemSrc.splice(itemSrc.indexOf(item), 1)
        itemSrc = [...itemSrc]
      },
      getItemInList: function (id, rows) {
        let itemRow = null
        rows.forEach(function (row) {
          // console.log('row.id: ' + row.id + ' - id: ' + id)
          if (row.id === id) {
            // console.log('row.id: ' + JSON.stringify(row))
            itemRow = row
          }
        })
        return itemRow
      },
      getIdOfItems: function (rows) {
        let rowsId = []
        rows.forEach(function (row) {
          if (typeof row === 'object' && typeof row.id !== 'undefined') {
            rowsId.push(row.id)
          }
        })
        return rowsId
      },
      convertDateFlatpicker: function (item) {
        let optDate = item.optdateclair
        let dateclair = item.dateclair
        let date = ''
        // Range: "2018-05-10 12:00 au 2018-05-16 12:00"
        // Simple: "2018-05-18 20:00---23:59"
        // multiples: "2018-05-19 12:00; 2018-05-20 12:00; 2018-05-26 12:00; 2018-06-02 12:00; 2018-06-09 12:00; 2018-06-16 12:00; 2018-06-22 12:00"
        // let dateclair = item.dateclair
        switch (optDate) {
          case 'rangedate' :
            // Exemple: "2018-05-10 12:00 au 2018-05-16 12:00"
            // Date avant "au"
            let dateBegin = dateclair.substring(0, dateclair.indexOf('au'))
            let dateLast = dateclair.substring(dateclair.indexOf('au') + 2)
            // Nettoi
            dateBegin = dateBegin.trim()
            dateLast = dateLast.trim()
            // Convertir Y-m-d en d-m-Y H:i
            dateBegin = Vue.moment(dateBegin, 'YYYY-MM-DD').format('DD-MM-YYYY')
            dateLast = Vue.moment(dateLast, 'YYYY-MM-DD').format('DD-MM-YYYY')
            date = dateBegin + ' au ' + dateLast
            return date

          case 'multiplesdate' :
            // Exemple: "2018-05-19 12:00; 2018-05-20 12:00; 2018-05-26 12:00"
            // Array des dates
            let datesArr = dateclair.split(';')
            let datesNewArr = []
            datesArr.forEach(function (row) {
              // Nettoi
              row = row.trim()
              // Convertir Y-m-d HH:mm en d-m-Y
              row = Vue.moment(row, 'YYYY-MM-DD HH:mm').format('DD-MM-YYYY')
              // Ajouter dans datesNewArr
              datesNewArr.push(row)
            })
            // Grouper tout
            date = datesNewArr.join(', ')
            return date
          default :
            // Exemple: "2018-05-18 20:00---23:59"
            // Date avant ---23:59
            date = dateclair.substring(0, dateclair.indexOf('---'))
            // Convertir Y-m-d en d-m-Y H:i
            date = Vue.moment(date, 'YYYY-MM-DD HH:mm').format('DD-MM-YYYY HH:mm')
            return date
        }
      },
      addItems: function (items, itemAddedArr) {
        if (items.length <= 0) {
          return false
        }

        let itemArr = []
        if (itemAddedArr.length > 0) {
          for (let i = 0; i < itemAddedArr.length; i++) {
            let itemid = itemAddedArr[i].id
            itemArr.push(itemid)
          }
        }
        let isAdded = false
        for (let i = 0; i < items.length; i++) {
          let itemRow = items[i]
          let itemRowId = itemRow.id
          if (itemArr.length > 0 && itemArr.indexOf(itemRowId) >= 0) {
            isAdded = true
          }
          if (itemArr.length > 0 && itemArr.indexOf(itemRowId) < 0) {
            isAdded = false
          }
          if (!isAdded) {
            // console.log(itemRow)
            itemAddedArr.push(itemRow)
          }
        }
        if (itemAddedArr.length > 0) {
          itemArr = []
          for (let i = 0; i < itemAddedArr.length; i++) {
            let itemid = itemAddedArr[i].id
            itemArr.push(itemid)
          }
        }
      }
    }
  }
}
