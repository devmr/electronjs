import Vue from 'vue'
import Vuetify from 'vuetify'
// import VueI18n from 'vue-i18n'
import i18n from './lang/lang'
import 'vuetify/dist/vuetify.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueMoment from 'vue-moment'
import App from './App'
import router from './router'
import store from './store'
import VueLazyload from 'vue-lazyload'
import VueUtils from './plugins/util'
import VeeValidate from 'vee-validate'
// Helpers
import colors from 'vuetify/es5/util/colors'
import flatPickr from 'vue-flatpickr-component'
import 'flatpickr/dist/flatpickr.css'
import 'flatpickr/dist/themes/material_red.css'
import './assets/global.css'
import VueBus from 'vue-bus'
import VueCroppie from 'vue-croppie'

Vue.use(VueCroppie)
Vue.use(require('vue-shortkey'))
Vue.use(flatPickr)
Vue.use(VueLazyload)
Vue.use(VueUtils)
Vue.use(VeeValidate)
Vue.use(Vuetify, {
  theme: {
    primary: colors.red.darken1, // #E53935
    secondary: colors.red.lighten4, // #FFCDD2
    accent: colors.indigo.base // #3F51B5
  }
})
// Vue.use(VueI18n)
Vue.use(VueAxios, axios)
Vue.use(VueMoment)
Vue.use(VueLazyload, {
  preLoad: 1.3,
  error: 'dist/error.png',
  loading: 'http://samherbert.net/svg-loaders/svg-loaders/oval.svg',
  attempt: 1
})
Vue.use(VueBus)

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.config.productionTip = false

// usage: {{ file.size | formatFileSize }}
Vue.filter('formatFileSize', function (num) {
  // jacked from: https://github.com/sindresorhus/pretty-bytes
  if (typeof num !== 'number' || isNaN(num)) {
    throw new TypeError('Expected a number')
  }

  var exponent
  var unit
  var neg = num < 0
  var units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  if (neg) {
    num = -num
  }

  if (num < 1) {
    return (neg ? '-' : '') + num + ' B'
  }

  exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1)
  num = (num / Math.pow(1000, exponent)).toFixed(2) * 1
  unit = units[exponent]

  return (neg ? '-' : '') + num + ' ' + unit
})

/* eslint-disable no-new */
new Vue({
  components: {
    App
  },
  router,
  store,
  i18n,
  template: '<App/>'
}).$mount('#app')
