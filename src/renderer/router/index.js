import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/welcome',
      name: 'welcome-view',
      component: require('@/components/WelcomeView').default
    },
    // Events
    {
      path: '/event/creer',
      name: 'event_creer',
      component: require('@/components/EventCreateEditView').default
    },
    {
      path: '/event/:id/edit',
      name: 'event_edit',
      component: require('@/components/EventCreateEditView').default
    },
    {
      path: '/',
      name: 'event_list',
      component: require('@/components/EventListView').default
    },
    {
      path: '/event/envoyesinternautes',
      name: 'event_envoyes_internautes',
      component: require('@/components/EventEnvoyesMembresView').default
    },
    // Lieux
    {
      path: '/lieux/creer',
      name: 'lieux_creer',
      component: require('@/components/LieuxCreerView').default
    },
    {
      path: '/lieux/list',
      name: 'lieux_list',
      component: require('@/components/LieuxListView').default
    },
    // Rubriques
    {
      path: '/rubriques/creer',
      name: 'rubriques_creer',
      component: require('@/components/RubriquesCreerView').default
    },
    {
      path: '/rubriques/list',
      name: 'rubriques_list',
      component: require('@/components/RubriquesListView').default
    },
    // Membres / Abonnes
    {
      path: '/users/listemembres',
      name: 'users_listemembres',
      component: require('@/components/UsersListmembresView').default
    },
    {
      path: '/users/listenewsletter',
      name: 'users_listenewsletter',
      component: require('@/components/UsersListnewslettersView').default
    },
    // Types d'entreees
    {
      path: '/typesentrees/creer',
      name: 'typesentrees_creer',
      component: require('@/components/TypesentreesCreerView').default
    },
    {
      path: '/typesentrees/list',
      name: 'typesentrees_list',
      component: require('@/components/TypesentreesListView').default
    },
    // Fetes
    {
      path: '/fetes/creer',
      name: 'fetes_creer',
      component: require('@/components/FetesCreerView').default
    },
    {
      path: '/fetes/list',
      name: 'fetes_list',
      component: require('@/components/FetesListView').default
    },
    // Artistes
    {
      path: '/artistes/creer',
      name: 'artistes_creer',
      component: require('@/components/ArtistesCreerView').default
    },
    {
      path: '/artistes/list',
      name: 'artistes_list',
      component: require('@/components/ArtistesListView').default
    },
    {
      path: '/settings',
      name: 'settings',
      component: require('@/components/SettingsView').default
    },
    {
      path: '/map',
      name: 'map',
      component: require('@/components/MapView').default
    },
    {
      path: '/map/:id',
      name: 'mapgps',
      component: require('@/components/MapView').default
    }
  ]
})
