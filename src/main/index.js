'use strict'

import { app, BrowserWindow } from 'electron'
const {ipcMain} = require('electron')
/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
let mapWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`
const mapURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080/#/map`
  : `file://${__dirname}/index.html#map`

function createMainWindow () {
  // console.log('__dirname: ' + `file://${__dirname}/index.html`)
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
    backgroundColor: '#E53935',
    title: 'Tsarabackend'
  })

  mainWindow.setMenu(null) // Cacher le menu

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}
function createMapWindow () {
  mapWindow = new BrowserWindow({
    height: 563,
    useContentSize: true,
    width: 1000,
    backgroundColor: '#E53935',
    title: 'Tsarabackend'
  })

  mapWindow.setMenu(null) // Cacher le menu

  mapWindow.loadURL(mapURL)

  mapWindow.hide()

  mapWindow.on('closed', () => {
    mapWindow = null
    createMapWindow()
  })
}
function createWindow () {
  createMainWindow()
  createMapWindow()
}
app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

ipcMain.on('ping', (event, data) => {
  event.sender.send('pong', Math.random())
})

ipcMain.on('setPopupView', (event, data) => {
  event.sender.send('initPopupView', true)
})
ipcMain.on('openPopup', (event, data) => {
  let popupUrl = process.env.NODE_ENV === 'development'
    ? `http://localhost:9080/#/map/${data}`
    : `file://${__dirname}/index.html#map/${data}`
  /* console.log(data)
  console.log(popupUrl) */
  mapWindow.loadURL(popupUrl)
  mapWindow.show()
})
ipcMain.on('gpsData', (event, data) => {
  event.sender.send('sendGpsData', data)
})
ipcMain.on('closePopup', (event, data) => {
  mapWindow.close()
})
ipcMain.on('getGpsData', (event, data) => {
  console.log('getGpsData: ' + data)
  event.sender.send('setMapGpsData', data)
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
